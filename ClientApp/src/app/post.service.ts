import { Post } from "./post.model";


export class PostService {
  private myArray: Post[] = [new Post("Magna Moris Utlicies", "./images/footer-a1.jpg", "12 nov 2017", 4),
    new Post("Ornare Nullam Risus", "./images/footer-a2.jpg", "12 nov 2017", 4),
    new Post("Eismud Nullam", "./images/footer-a3.jpg", "12 nov 2017", 4)];

  getPosts() {
    return this.myArray.slice();
  }
  getPostsNumbered(num:number) {
    return this.myArray.slice(0,num);
  }
  getPost(index: number) {
    return this.myArray[index];
  }
  addPost(p: Post) {
    this.myArray.push(p);
  }
}
