export class Post {
  public title: string;
  public img: string;
  public date: string;
  public numOfReplies: number;

  constructor(t: string, i: string, d: string, n: number) {
    this.title = t;
    this.img = i;
    this.date = d;
    this.numOfReplies = n;
  }
}
