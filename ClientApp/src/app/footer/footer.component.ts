import { Component } from '@angular/core';
import { Post } from '../post.model';
import { PostService } from '../post.service';
import { faClock, faComment } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  private posts: Post[];

  constructor(private postService: PostService) {
    this.posts = this.postService.getPostsNumbered(3);//getting posts needed to display on page
  }

  //Font Awesome icons
  faClock = faClock;
  faComment = faComment;
}
