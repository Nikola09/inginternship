export class Customer {
  public name: string;
  public img: string;
  public job: string;
  public story: string;

  constructor(name: string, img: string,job:string,story:string) {
    this.name = name;
    this.img = img;
    this.job = job;
    this.story = story;
  }
}
