import { Component } from '@angular/core';
import { faSearch, faShareAlt, faTasks, faThLarge, faCheck, faLightbulb, faFolder,faHeart } from '@fortawesome/free-solid-svg-icons';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [trigger('rocket', [  // animation for rocket flying up 100px, had to balance this with class rocket-balance
    state('normal', style({ transform: 'translateY(-100px)' })),
    transition('void => normal', [
      animate('3s', style({ opacity: 0.9, transform: 'translateY(-100px)'}))
    ]),
  ])]
		
})
export class HomeComponent {
  private customers: Customer[];
  state = 'normal'; //state for rocket
  constructor(private customerService: CustomerService) {
    this.customers = this.customerService.getCustomersNumbered(3); //getting customers needed to display on page
  }

  //Font Awesome icons
  faSearch = faSearch;
  faShareAlt = faShareAlt;
  faTasks = faTasks;
  faThLarge = faThLarge;
  faCheck = faCheck;
  faLightbulb = faLightbulb;
  faFolder = faFolder;
  faHeart = faHeart;
}
