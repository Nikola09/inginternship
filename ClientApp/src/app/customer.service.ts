import { Customer } from "./customer.model";

export class CustomerService {
  private myArray: Customer[] = [new Customer("Connor Gibson", "./images/customer-t1.jpg","Financial Analyst","Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis, est non commodo, luctus nisi erat portitol ligula."),
  new Customer("Coriss Ambady", "./images/customer-t2.jpg","Marketing Specialist","Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula porta felis euismod semper. Cras justo odio, dapibus ac facilisis."),
  new Customer("Barclay Widerski", "./images/customer-t3.jpg","Sales Manager","Duis mollis, est non commodo luctus, nisi erat portitor lugila, eget lacinia, odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam venenatis."),
  new Customer("customer4", "./images/customer-t4.jpg", "unknown", "unknown"),
  new Customer("customer5", "./images/customer-t5.jpg", "unknown", "unknown"),
  new Customer("customer6", "./images/customer-t6.jpg", "unknown", "unknown")];

  getCustomers() {
    return this.myArray.slice();
  }
  getCustomersNumbered(num: number) {
    return this.myArray.slice(0, num);
  }
  getCustomer(index: number) {
    return this.myArray[index];
  }
  addCustomer(c: Customer) {
    this.myArray.push(c);
  }

  addCustomers(cs: Customer[]) {
    this.myArray.push(...cs);
  }
}
